### I am [g]ROOT?

[![build status](http://git.doit.wisc.edu/ci/projects/8/status.png?ref=master)](http://git.doit.wisc.edu/ci/projects/8?ref=master)

This is the root context of myuw. It is just here to serve up some static html pages for 500 and 400 level errors.
